import React, { Component } from 'react';
import Item from './Item'
import css from './App.css'



class App extends Component {

  state = {
    inputValue: 'default value',
    todos: [],
    value:''
  }
  componentDidMount() {

  }
  
  render() {
    return (
      <div className="App">
      <h1>To Do List</h1>
      <input id='input' placeholder="enter task" onChange={(e) => {
        this.setState({ inputValue: e.target.value})
        console.log(e.target.value)
      }}></input>
      <button onClick={() => {
        // add input value to todos array
        const arr = this.state.todos
        arr.push(this.state.inputValue)
       
        this.setState({ todos: arr })
        console.log('todos:',this.state.todos)
        // this.state.todos.push(this.state.inputValue)
        document.getElementById('input').value=''
        
      }}>Add</button>
      <button onClick={()=>{
        this.setState({todos:[]})
        
      }}>Clear All</button>
      
      <div> 
        {this.state.todos.map((itemToDo, ind) => {
          return <Item key={ind} unique={ind} item={itemToDo} parent={this} onEditFinish={(val) => {
            const arr = this.state.todos
            arr[ind]=val
            this.setState({todos:arr})
            
            console.log('todos', this.state.todos)
          }}
          onDeleteClick={(val1)=>{
            const arr=this.state.todos
            
            arr.splice(val1,1)
            
            this.setState({todos:arr})
            console.log('todos',this.state.todos)
          }}
          ></Item>
        })}
      </div>
      </div>
    );
  }
}

export default App;
