import React from 'react'



export default class Item extends React.Component {
    state = {
        isCheck:false,
        todos1:[]
    }
    
    render() {
        return <div id="parent"
        >
            {(this.state.isCheck)?<del>{this.props.item}</del>:<b id={"id"+this.props.unique} 
            onDoubleClick={()=>{document.getElementById("id"+this.props.unique).contentEditable="true"}}
            onKeyDown={(event)=>{
            
            if(event.keyCode===13) {
                event.preventDefault()
               
                if(this.props.onEditFinish) {
                 
                    this.props.onEditFinish(document.getElementById("id"+this.props.unique).innerText)
                }
                document.getElementById("id"+this.props.unique).contentEditable="false"
            }
            
        }}>{this.props.item}</b>}
            <input type="checkbox" onClick={()=>this.setState({isCheck:!this.state.isCheck})}></input>
            <button  onClick={()=>{
                
                if(this.props.onDeleteClick) {
                    
                    this.props.onDeleteClick(this.props.unique)

                }
                
                
            }}>Delete</button>
        </div>
       
        /*</div>
        return <div>
            <div onClick={()=>this.setState({isCheck: !this.state.isCheck})}>V</div>
            {(this.state.isCheck) ? <del>{this.props.item}</del> : <div>{this.props.item}</div>}
        </div>
        */
    }
}
